"""
Quick manual backup script I guess

Manually poll all results from DB I guess
"""

import time
import datetime
import sqlite3
import os
import uuid
import copy
import argparse
import sys
import json

import influxdb



def dump_environment_2(influx_client : influxdb.InfluxDBClient, sqlite_path):
    """
    Previous script had issues on large DBs, connection gets reset by peer and we get empty chunks that cause exceptions?
    Also, we we dump via offset the query time gets progressively longer.

    Assume there's an index on time and try that?
    """
    # This try block mostly to make sure the DB being written is comitted
    try:
        conn = sqlite3.connect(sqlite_path)
        cursor = conn.cursor()

        # Assume the columns don't change, which isn't necessarily true
        create_statement = """
        CREATE TABLE IF NOT EXISTS bme_environment
        (
            pk_environment_id INTEGER PRIMARY KEY,
            time TEXT,
            dew_pt REAL,
            humidity REAL,
            pressure REAL,
            python_timestamp REAL,
            sensor_id TEXT,
            sensor_location TEXT,
            temperature REAL
        )
        """
        cursor.execute(create_statement)

        # Find minimum time, we'll go by days I guess
        minimum_time = influx_client.query("SELECT MIN(time) FROM bme_environment")
        number_values = influx_client.query("SELECT COUNT(temperature) FROM bme_environment")
        # Select after 2020; system definitely not running while I'm in France
        # Limit 5 just to quickly not blowup

        select_statement = """
        SELECT time, temperature
        FROM bme_environment
        WHERE time > '2020-01-01'
        ORDER BY time ASC
        LIMIT 5
        """
        select_result = influx_client.query(select_statement)
        print(minimum_time)
        print(number_values)
        print(f"Select result: {select_result}")
        # Get minimum day and we'll go from there
        now = datetime.datetime.utcnow()
        minimum_time = [i for i in select_result["bme_environment"]][0]["time"]
        print(f"Start backup from {minimum_time}")
        minimum_date = datetime.datetime.strptime(minimum_time[:10], "%Y-%m-%d")
        date_from = minimum_date
        date_to = minimum_date + datetime.timedelta(days=1)
        select_statement = """
        SELECT time, dew_pt, pressure, python_timestamp, sensor_id, sensor_location, temperature, humidity
        FROM bme_environment
        WHERE
        (
            time > $date_from
            AND time < $date_to
        )
        """
        sqlite_insert_statement = """
        INSERT INTO bme_environment
        (
            time,
            dew_pt,
            humidity,
            pressure,
            python_timestamp,
            sensor_id,
            sensor_location,
            temperature
        )
        VALUES
        (
            :time,
            :dew_pt,
            :humidity,
            :pressure,
            :python_timestamp,
            :sensor_id,
            :sensor_location,
            :temperature
        )
        """
        while (date_from < now):
            params = {
                "date_from" : date_from.strftime("%Y-%m-%d"),
                "date_to" : date_to.strftime("%Y-%m-%d"),
            }
            time_query_start = time.time()
            these_results = influx_client.query(query=select_statement, bind_params=params)
            time_query_end = time.time()
            insert_dicts = [i for i in these_results["bme_environment"]]
            for i in range(0, len(insert_dicts)):
                if "humidity" not in insert_dicts[i]:
                    insert_dicts[i]["humidity"] = None
            cursor.executemany(sqlite_insert_statement, insert_dicts)
            conn.commit()
            print(f"Searched from {date_from} to {date_to}")
            print("Time to query: {}".format(time_query_end - time_query_start))
            print("Number dicts {}".format(len(insert_dicts)))
            date_from = date_to
            date_to = date_to + datetime.timedelta(days=1)


    finally:
        conn.commit()
        conn.close()

    return

def dump_environment(influx_client, sqlite_path):
    """

    """
    # Do a quick select to get list of columns/tags whatever
    try:
        conn = sqlite3.connect(sqlite_path)
        cursor = conn.cursor()

        # Assume the columns don't change, which isn't necessarily true
        create_statement = """
        CREATE TABLE IF NOT EXISTS bme_environment
        (
            pk_environment_id INTEGER PRIMARY KEY,
            time TEXT,
            dew_pt REAL,
            humidity REAL,
            pressure REAL,
            python_timestamp REAL,
            sensor_id TEXT,
            sensor_location TEXT,
            temperature REAL
        )
        """
        cursor.execute(create_statement)

        # Now we start looping across DB to get values
        # Bit hacky b/c of how influx appears to work but still
        number_values = influx_client.query("SELECT COUNT(temperature) FROM bme_environment")
        number_values = [i for i in number_values["bme_environment"]][0]["count"]

        max_query_length = 100000
        influx_select_statement = """
        SELECT time, dew_pt, pressure, python_timestamp, sensor_id, sensor_location, temperature, humidity
        FROM bme_environment[]
        ORDER BY time ASC
        LIMIT {}
        OFFSET {}
        """

        sqlite_insert_statement = """
        INSERT INTO bme_environment
        (
            time,
            dew_pt,
            humidity,
            pressure,
            python_timestamp,
            sensor_id,
            sensor_location,
            temperature
        )
        VALUES
        (
            :time,
            :dew_pt,
            :humidity,
            :pressure,
            :python_timestamp,
            :sensor_id,
            :sensor_location,
            :temperature
        )
        """
        # Now scroll through and dump
        this_offset = 0
        while this_offset < number_values:
            time_query_start = time.time()
            these_results = influx_client.query(influx_select_statement.format(max_query_length,this_offset))
            time_query_end = time.time()
            insert_dicts = [i for i in these_results["bme_environment"]]
            for i in range(0, len(insert_dicts)):
                if "humidity" not in insert_dicts[i]:
                    insert_dicts[i]["humidity"] = None
            cursor.executemany(sqlite_insert_statement, insert_dicts)
            conn.commit()
            this_offset = this_offset + max_query_length
            print("Offset {} of {} results, fraction {:.2g}".format(this_offset, number_values, this_offset / number_values))
            print("Time to query: {}".format(time_query_end - time_query_start))
            print("Number dicts {}".format(len(insert_dicts)))


    finally:
        conn.commit()
        conn.close()

    return

def dump_bom_data(influx_client, sqlite_path):
    """

    """
    try:
        conn = sqlite3.connect(sqlite_path)
        cursor = conn.cursor()
        # Assume the columns don't change, which isn't necessarily true
        # Use same values as in bom data
        create_statement = """
        CREATE TABLE IF NOT EXISTS bom_data
        (
            pk_bom_id INTEGER PRIMARY KEY,
            time TEXT,
            air_temp REAL,
            dew_pt REAL,
            name TEXT,
            press REAL,
            rel_hum REAL
        )
        """
        cursor.execute(create_statement)

        # Now we start looping across DB to get values
        # Bit hacky b/c of how influx appears to work but still
        number_values = influx_client.query("SELECT COUNT(air_temp) FROM bom_data")
        number_values = [i for i in number_values["bom_data"]][0]["count"]

        max_query_length = 100000
        influx_select_statement = """
        SELECT time, air_temp, dew_pt, "name", press, rel_hum
        FROM bom_data
        ORDER BY time ASC
        LIMIT {}
        OFFSET {}
        """

        sqlite_insert_statement = """
        INSERT INTO bom_data
        (
            time,
            air_temp,
            dew_pt,
            name,
            press,
            rel_hum
        )
        VALUES
        (
            :time,
            :air_temp,
            :dew_pt,
            :name,
            :press,
            :rel_hum
        )
        """
        # Now scroll through and dump
        this_offset = 0
        while this_offset < number_values:
            time_query_start = time.time()
            these_results = influx_client.query(influx_select_statement.format(max_query_length,this_offset))
            time_query_end = time.time()
            insert_dicts = [i for i in these_results["bom_data"]]
            cursor.executemany(sqlite_insert_statement, insert_dicts)
            conn.commit()
            this_offset = this_offset + max_query_length
            print("Offset {} of {} results, fraction {:.2g}".format(this_offset, number_values, this_offset / number_values))
            print("Time to query: {}".format(time_query_end - time_query_start))
            print("Number dicts {}".format(len(insert_dicts)))


    finally:
        conn.commit()
        conn.close()

    return

    return

def main():
    """
    Main entry point
    """
    parser = argparse.ArgumentParser()
    parser.add_argument("sqlite_path", help="Path to sqlite database to dump to, DOESN'T SYNC YET")
    parser.add_argument("config_json_path", help="Path to a JSON file with config deets")
    parser.add_argument("--skip_bom", action="store_true", help="Whether to skip backing up the BOM tables")
    parser.add_argument("--use_old_method", action="store_true", help="Whether to use the old dumping method")
    args = parser.parse_args()

    with open(args.config_json_path, 'r') as a_file:
        config_json = json.load(a_file)
    influx_host = config_json["INFLUX_HOST"]
    influx_port = config_json["INFLUX_PORT"]
    influx_path = config_json["INFLUX_PATH"]


    # Print out databases, table names, and assume we know ahead of time what we want
    influx_client = influxdb.InfluxDBClient(host=influx_host, port=influx_port, path=influx_path, timeout=60, retries=10)
    print("Databases present: {}".format(influx_client.get_list_database()))
    database_to_dump = "sensor_logs_weather"
    print("Switching to {}".format(database_to_dump))
    influx_client.switch_database(database_to_dump)
    print("Measurements present: {}".format(influx_client.get_list_measurements()))

    print("Dumping environment")
    if args.use_old_method:
        dump_environment(influx_client, args.sqlite_path)
    else:
        dump_environment_2(influx_client, args.sqlite_path)

    if not args.skip_bom:
        print("Dumping bom data")
        dump_bom_data(influx_client, args.sqlite_path)

    print("End of backup, quitting")

    sys.exit(0)

if __name__ == "__main__":
    main()
